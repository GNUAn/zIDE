# zIDE
zIDE is a CLI IDE that uses texor text editor as the base.
It is made for fast software development.

## Licensing
zIDE is unlicensed software.
Original Texor is licensed under BSD 2-clause license.
