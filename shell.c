#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include "texor.c"
#include <sys/stat.h>
#include <fcntl.h>
void shell(void);
void shell(void)
{
  char buf[64];
  char buf2[128];
  int i;
  while(1)
  {
    printf("> ");
    fgets(buf, 1024, stdin);
    buf[strlen(buf)-1] = '\0';
    if(strcmp(buf, "exit") == 0 || strcmp(buf, "q") == 0)
    {
      exit(0);
    }
    else if(strcmp(buf, "c") == 0)
    {
      printf("Type a name of file to create:\t");
      fgets(buf2, 1024, stdin);
      buf2[strlen(buf2)-1] = '\0';
      creat(buf2, 0777);
      printf("File %s created\n", buf2);
    }
    else if (strcmp(buf, "man") == 0)
    {
      printf("What man page do you want to read?\n");
      fgets(buf2, 1024, stdin);
      buf2[strlen(buf2)-1] = '\0';
      int pid = fork();
      if (pid == 0)
      {
        execlp("man", "man", buf2, NULL);
      }
      else if (pid == -1)
      {
        printf("Fork failed\n");
      }
      else
      {
        waitpid(-1, NULL, 0);
      }
    }
    else if (strcmp(buf, "cc") == 0)
    {
      printf("Type a name of directory to create:\t");
      fgets(buf2, 1024, stdin);
      buf2[strlen(buf2)-1] = '\0';
      mkdir(buf2, 0777);
      printf("Directory %s created\n", buf2);
    }
    else if (strcmp(buf, "mk") == 0)
    {
      if (fopen("Makefile", "r") == NULL)
      {
        printf("Makefile not found\nTry writing Makefile\n");
      }
      else
      {
        system("make");
      }
    }
    else if(strcmp(buf, "l") == 0)
    {
      system("ls");
    }
    else if (strcmp(buf, "ll") == 0)
    {
      system("ls -l");
    }
    else if(strcmp(buf, "cd") == 0)
    {
      printf("Type a directory name:\t");
      fgets(buf2, 1024, stdin);
      buf2[strlen(buf2)-1] = '\0';
      if(chdir(buf2) == -1)
      {
        printf("Directory %s not found\n", buf2);
      }
      else
      {
        printf("Directory changed to %s\n", buf2);
      }
    }
    else if(strcmp(buf, "sh") == 0)
    {
      int pid = fork();
      if(pid == 0)
      {
        execv("/bin/sh", NULL);
      }
      else
      {
        wait(NULL);
      }
    }
    else if(strcmp(buf, "e") == 0)
    {
      printf("Type name of file that you want to edit\n");
      fgets(buf2, 1024, stdin);
      buf2[strlen(buf2)-1] = '\0';
      int pid = fork();
      int wstatus;
      if (pid == 0)
      {
        texor(buf2);
      }
      else
      {
        wait(NULL);
      }
    }
    else if(strcmp(buf, "exec") == 0)
      {
	printf("What command do you want to execute?\n");
	fgets(buf2, 1024, stdin);
        buf2[strlen(buf2)-1] = '\0';
	system(buf2);
      }
    else if (strcmp(buf, "help") == 0)
      {
	printf("q or exit - exit zIDE\n\nc - create file\ncc - create directory\nmk - execute \"make\"\nl or ll - print list of files and sub-directories\ncd - change directory\nman - read manual pages\nsh - go to standard shell\nexec - execute a command\ne - edit file using texor\n");
      }
  }
}
int main(void)
{
  shell();
	printf("Thank you for using zIDE!");
  return 0;
}
