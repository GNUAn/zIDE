# Shell documentation
zIDE shell is very simple to use.
##### List of commands
l - execute "ls"
ll - execute "ls -l"
sh - go to shell(/bin/sh)
c - create file
cc - create directory
cd - go to directory
mk - execute "make"
man - read man pages
e - edit file using texor.

  Copyright (c)  YEAR  YOUR NAME.
  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in file called "fdl.txt".